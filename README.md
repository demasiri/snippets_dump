# Roll20.net Macros

## Hunter

### Roll Attack (Longbow)
Option for Sharpshooter available.

`&{template:atk} {{mod=+5}} {{rname=[Longbow](!&#13;#atk-bow)}} {{r1=[[1d20cs>20 + @{Aran Cromwin|dexterity_mod}[DEX] + 2[PROF] - ?{Sharpshooter?|Yes,5|No,0}[SHRP]]]}} {{normal=1}} {{charname=Aran Cromwin}}`

### Roll Damage (Longbow)
This allows you to select whether Sharpshooter damage should be applied and whether Hunter's Mark damage should apply.

If Sharpshooter is active, adds 10 to the damage as per Feat details.

If HM is active, this rolls a second damage score. Otherwise only the initial Piercing damage is rolled.
`&{template:dmg} {{range=600ft}} {{damage=1}} {{dmg1flag=1}} {{dmg1=[[1d8 + @{Aran Cromwin|dexterity_mod}[DEX] + ?{Sharpshooter |No, 0|Yes, 10}[SHRP] ]] }} {{dmg1type=Piercing}} {{charname=Aran Cromwin}} ?{Hunter's Mark |No,&#123;&#123;dmg2flag=&#125;&#125;|Yes,&#123;&#123;dmg2flag=1&#125;&#125;} {{dmg2=[[1d6[H.Mk]]]}} {{dmg2type=Hunter's Mark}}`




### Roll It All
`&{template:default} {{name=Longbow}} {{attack=[[1d20cs>20 + @{Aran Cromwin|dexterity_mod}[Dex Mod] + 2[Prof]]]}} {{damage=[[1d8 + @{Aran Cromwin|dexterity_mod}]]}} {{if crit=[[1d8]]}} {{if hunter's mark=[[1d6]]}}`

# Longsword Attack
`&{template:default} {{name=Longsword +1}} {{Roll=[[1d20cs>20 + @{Aran Cromwin|strength_mod}[StrMod] + @{Aran Cromwin|pb}[Prof] + 1[WeapBonus]]] | [[1d20cs>20 + @{Aran Cromwin|strength_mod}[StrMod] + @{Aran Cromwin|pb}[Prof] + 1[WeapBonus]]]}} {{Properties=Versatile (1d10)}} {{Bonuses=+1 Atk (see roll)&#13;+ 1 Dmg}} {{Rolls=[Damage](!&#13;#dmg-ls1) [Crit](!&#13;#crit-ls1)}}`

# Short Sword Attack
`&{template:default} {{name=Shortsword}} {{Roll=[[1d20cs>20 + @{Aran Cromwin|dexterity_mod}[DexMod] + @{Aran Cromwin|pb}[Prof]]] | [[1d20cs>20 + @{Aran Cromwin|dexterity_mod}[DexMod] + @{Aran Cromwin|pb}[Prof]]]}} {{Properties=*Finesse, Light*}} {{Rolls=[Damage](!&#13;#dmg-ss1) [Crit](!&#13;#crit-ss1)}}`

## Short Sword DMG & HM
`&{template:default} {{name=Shortsword}} {{Damage Base=[[1d6 + @{Aran Cromwin|dexterity_mod}[DexMod]]]}} ?{Hunter's Mark|No,|Yes,&#123;&#123;Hunter's Mark=&#91;&#91;1d6&#93;&#93;&#125;&#125;}`

## Short Sword Crit & HM Optional
`&{template:default} {{name=Shortsword Critical}} {{Damage Base=[[2d6 + @{Aran Cromwin|dexterity_mod}[DexMod]]]}} ?{Hunter's Mark|No,|Yes,&#123;&#123;Hunter's Mark=&#91;&#91;1d6&#93;&#93;&#125;&#125;}`