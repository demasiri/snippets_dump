## Wild Magic Percentage Chance
This is the table that I will be rolling against as a DM. You won't need to worry about this but it's good that you see and understand what I'll be working from!

|Sorcerer Level|Base Wild Chance|
|:-|-:|
|1|30%|
|5|35%|
|10|40%|
|15|45%|
|20|50%|

Casting a spell, you will have a chance to trigger Wild Magic. This chance is the Base Wild Chance + Spell Slot Level. Example: You're level 5 and cast Scorching Ray (Lvl2 Spell) using a Lvl3 slot, the Wild Magic chance will be 38. I will roll a d100 when you cast the spell, if it's 38 or lower, you will trigger Wild Magic and have to roll for an effect.

## Effect Table Macro

```
!power {{
--name|Wild Mage Roll
--whisper|GM, Iris
--Roll|[[ [$WM] 1d100 ]]
--?? $WM.base == 1 OR $WM.base == 2 ?? Effect|Roll on this table at the start of each of your turns for the next minute, ignoring this result on subsequent rolls.
--?? $WM.base == 3 OR $WM.base == 4 ?? Effect|For the next minute, you can see any invisible creature if you have line of sight to it.
--?? $WM.base == 5 OR $WM.base == 6 ?? Effect|A modron chosen and controlled by the DM appears in an unoccupied space within 5 feet of you, then disappears 1 minute later.
--?? $WM.base == 7 OR $WM.base == 8 ?? Effect|You cast fireball as a 3rd-level spell centered on yourself.
--?? $WM.base == 9 OR $WM.base == 10 ?? Effect|You cast magic missile as a 5th-level spell.
--?? $WM.base == 11 OR $WM.base == 12 ?? Effect|Roll a d10. Your height changes by a number of inches equal to the roll. If the roll is odd, you shrink. If the roll is even, you grow.
--?? $WM.base == 13 OR $WM.base == 14 ?? Effect|You cast confusion centered on yourself.
--?? $WM.base == 15 OR $WM.base == 16 ?? Effect|For the next minute, you regain 5 hit points at the start of each of your turns.
--?? $WM.base == 17 OR $WM.base == 18 ?? Effect|You grow a long beard made of feathers that remains until you sneeze, at which point the feathers explode out from your face.
--?? $WM.base == 19 OR $WM.base == 20 ?? Effect|You cast grease centered on yourself.
--?? $WM.base == 21 OR $WM.base == 22 ?? Effect|Creatures have disadvantage on saving throws against the next spell you cast in the next minute that involves a saving throw.
--?? $WM.base == 23 OR $WM.base == 24 ?? Effect|Your skin turns a vibrant shade of blue. A remove curse spell can end this effect.
--?? $WM.base == 25 OR $WM.base == 26 ?? Effect|An eye appears on your forehead for the next minute.
--?? $WM.base == 27 OR $WM.base == 28 ?? Effect|For the next minute, all your spells with a casting time feet of 1 action have a casting time of 1 bonus action.
--?? $WM.base == 29 OR $WM.base == 30 ?? Effect|You teleport up to 60 feet to an unoccupied space of your choice that you can see.
--?? $WM.base == 31 OR $WM.base == 32 ?? Effect|You are transported to the Astral Plane until the end of your next turn, after which time you return to the space you previously occupied or the nearest unoccupied space if that space is occupied.
--?? $WM.base == 33 OR $WM.base == 34 ?? Effect|Maximize the damage of the next damaging spell you cast within the next minute.
--?? $WM.base == 35 OR $WM.base == 36 ?? Effect|Roll a d10. Your age changes by a number of years equal to the roll. If the roll is odd, you get younger (minimum 1 year old). If the roll is even, you get older.
--?? $WM.base == 37 OR $WM.base == 38 ?? Effect|1d6 flumphs controlled by the DM appear in unoccupied spaces within 60 feet of you and are frightened of you. They vanish after 1 minute.
--?? $WM.base == 39 OR $WM.base == 40 ?? Effect|You regain 2d10 hit points.
--?? $WM.base == 41 OR $WM.base == 42 ?? Effect|You turn into a potted plant until the start of your next turn. While a plant, you are incapacitated and have vulnerability to all damage. If you drop to 0 hit points, your pot breaks, and your form reverts.
--?? $WM.base == 43 OR $WM.base == 44 ?? Effect|For the next minute, you can teleport up to 20 feet as a bonus action on each of your turns.
--?? $WM.base == 45 OR $WM.base == 46 ?? Effect|You cast levitate on yourself.
--?? $WM.base == 47 OR $WM.base == 48 ?? Effect|A unicorn controlled by the DM appears in a space within 5 feet of you, then disappears 1 minute later.
--?? $WM.base == 49 OR $WM.base == 50 ?? Effect|You can't speak for the next minute. Whenever you try, pink bubbles float out of your mouth.
--?? $WM.base == 51 OR $WM.base == 52 ?? Effect|A spectral shield hovers near you for the next minute, granting you a +2 bonus to AC and immunity to magic missile.
--?? $WM.base == 53 OR $WM.base == 54 ?? Effect|You are immune to being intoxicated by alcohol for the next 5d6 days.
--?? $WM.base == 55 OR $WM.base == 56 ?? Effect|Your hair falls out but grows back within 24 hours.
--?? $WM.base == 57 OR $WM.base == 58 ?? Effect|For the next minute, any flammable object you touch that isn't being worn or carried by another creature bursts into flame.
--?? $WM.base == 59 OR $WM.base == 60 ?? Effect|You regain your lowest-level expended spell slot.
--?? $WM.base == 61 OR $WM.base == 62 ?? Effect|For the next minute, you must shout when you speak.
--?? $WM.base == 63 OR $WM.base == 64 ?? Effect|You cast fog cloud centered on yourself.
--?? $WM.base == 65 OR $WM.base == 66 ?? Effect|Up to three creatures you choose within 30 feet of you take 4d10 lightning damage.
--?? $WM.base == 67 OR $WM.base == 68 ?? Effect|You are frightened by the nearest creature until the end of your next turn.
--?? $WM.base == 69 OR $WM.base == 70 ?? Effect|Each creature within 30 feet of you becomes invisible for the next minute. The invisibility ends on a creature when it attacks or casts a spell.
--?? $WM.base == 71 OR $WM.base == 72 ?? Effect|You gain resistance to all damage for the next minute.
--?? $WM.base == 73 OR $WM.base == 74 ?? Effect|A random creature within 60 feet of you becomes poisoned for 1d4 hours
--?? $WM.base == 75 OR $WM.base == 76 ?? Effect|You glow with bright light in a 30-foot radius for the next minute. Any creature that ends its turn within 5 feet of you is blinded until the end of its next turn.
--?? $WM.base == 77 OR $WM.base == 78 ?? Effect|Illusory butterflies and flower petals flutter in the air within 10 feet of you for the next minute.
--?? $WM.base == 79 OR $WM.base == 80 ?? Effect|You cast polymorph on yourself. If you fail the saving throw, you turn into a sheep for the spell's duration.
--?? $WM.base == 81 OR $WM.base == 82 ?? Effect|You can take one additional action immediately.
--?? $WM.base == 83 OR $WM.base == 84 ?? Effect|Each creature within 30 feet of you takes 1d10 necrotic damage. You regain hit points equal to the sum of the necrotic damage dealt.
--?? $WM.base == 85 OR $WM.base == 86 ?? Effect|You cast mirror image.
--?? $WM.base == 87 OR $WM.base == 88 ?? Effect|You cast fly on a random creature within 60 feet of you.
--?? $WM.base == 89 OR $WM.base == 90 ?? Effect|You become invisible for the next minute. During that time, other creatures can't hear you. The invisibility ends if you attack or cast a spell
--?? $WM.base == 91 OR $WM.base == 92 ?? Effect|If you die within the next minute, you immediately come back to life as if by the reincarnate spell.
--?? $WM.base == 93 OR $WM.base == 94 ?? Effect|Your size increases by one size category for the next minute.
--?? $WM.base == 95 OR $WM.base == 96 ?? Effect|You and all creatures within 30 feet of you gain vulnerability to piercing damage for the next minute.
--?? $WM.base == 97 OR $WM.base == 98 ?? Effect|You are surrounded by faint, ethereal music for the next minute.
--?? $WM.base == 99 OR $WM.base == 100 ?? Effect|You regain all expended sorcery points.
}}
```