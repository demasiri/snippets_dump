# Power Cards

## Attack with Alterbar

```
!power {{
--tokenid|@{selected|token_id}
--emote|**@{selected|token_name}** lunges for @{target|character_name}'s throat
--name|Griffes
--leftsub|Action
--rightsub|Cac 1.5m
--target_list|@{target|token_id}
--Attaque *1|[[ [$Atk1] ?{Advantage?| Normal, 1d20 + [[4]] | Advantage, 2d20kh1 + [[4]] | Disadvantage, 2d20kl1 + [[4]] [PROF]]} ]] vs AC
--?? $Atk1.base == 1 OR $Atk1.total < @{target|AC}?? !Miss*1:|Echec.
--?? $Atk1 < @{target|AC} ?? soundfx*1|_play|miss
--?? $Atk1 >= @{target|AC} ?? soundfx*2|_play|bite
--?? $Atk1.total >= @{target|AC} AND $Atk1.base <> 1 AND $Atk1.base <> 20 ?? Dégâts*1:|[[ [$Dmg1] 1d6 + 3]] Piercing
--?? $Atk1.base == 20 ?? Coup critique*1:|[[ [$CritDmg1] 2d6 +3]] Piercing
--Attaque *2|[[ [$Atk2] ?{Advantage?| Normal, 1d20 + [[4]] | Advantage, 2d20kh1 + [[4]] | Disadvantage, 2d20kl1 + [[4]] [PROF]]} ]] vs AC
--?? $Atk2.base == 1 OR $Atk2.total < @{target|AC}?? !Miss*2:|Echec.
--?? $Atk2 < @{target|AC} ?? soundfx*3|_play|miss
--?? $Atk2 >= @{target|AC} ?? soundfx*4|_play|bite
--?? $Atk2.total >= @{target|AC} AND $Atk2.base <> 1 AND $Atk2.base <> 20 ?? Dégâts*2:|[[ [$Dmg2] 1d6 + 3]] Piercing
--?? $Atk2.base == 20 ?? Coup critique*2:|[[ [$CritDmg2] 2d6 +3]] Dommage Tranchant

--?? $Atk1.total >= @{target|AC} AND $Atk1.base <> 1 AND $Atk1.base <> 20 AND $Atk2.total >= @{target|AC} AND $Atk2.base <> 1 AND $Atk2.base <> 20 ?? alterbar*1|_target|@{target|token_id} _bar|1 _amount|-[^Dmg1] + [^Dmg2] _show|all
--?? $Atk1.total >= @{target|AC} AND $Atk1.base <> 1 AND $Atk1.base <> 20 ?? alterbar*1|_target|@{target|token_id} _bar|1 _amount|-[^Dmg1] _show|all
--?? $Atk2.total >= @{target|AC} AND $Atk2.base <> 1 AND $Atk2.base <> 20 ?? alterbar*1|_target|@{target|token_id} _bar|1 _amount|-[^Dmg2] _show|all
--?? $Atk1.base == 20 ?? alterbar*2|_target|@{target|token_id} _bar|1 _amount|-[^CritDmg1] _show|all
--?? $Atk2.base == 20 ?? alterbar*2|_target|@{target|token_id} _bar|1 _amount|-[^CritDmg2] _show|all
--?? $Atk1.base == 20 AND $Atk2.base == 20 ?? alterbar*2|_target|@{target|token_id} _bar|1 _amount|-[^CritDmg1] + [^CritDmg2] _show|all
}}
```

```
!power{{
  --tokenid|@{selected|token_id}
  --target_list|@{target|token_id}
  --alterbar*1|_target|@{target|token_id} _bar|1 _amount|-?{Damage Taken|0} _show|all
  --api_token-mod|_ids @{selected|token_id} _ignore-selected _set statusmarkers|stopwatch
}}
```